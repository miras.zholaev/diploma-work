.PHONY : swiftshield

swiftshield:
	cd ../ && make build && make package
	../.build/release/_PRODUCT/bin/swiftshield obfuscate -p ./diploma.xcodeproj -s diploma -v
	open ./diploma.xcodeproj
	open ./swiftshield-output
