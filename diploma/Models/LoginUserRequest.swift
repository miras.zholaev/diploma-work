//
//  LoginUserRequest.swift
//  diploma
//
//  Created by Toremurat Zholayev on 24.04.2024.
//

import Foundation

struct LoginUserRequest {
    let email: String
    let password: String
}
