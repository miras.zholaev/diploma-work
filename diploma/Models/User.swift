//
//  User.swift
//  diploma
//
//  Created by Toremurat Zholayev on 11.06.2024.
//

import Foundation

struct User {
    let username: String
    let email: String
    let userUID: String
}
