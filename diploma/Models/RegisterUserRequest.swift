//
//  RegisterUserRequest.swift
//  diploma
//
//  Created by Toremurat Zholayev on 23.04.2024.
//

import Foundation

struct RegisterUserRequest {
    let username: String
    let email: String
    let password: String
}
