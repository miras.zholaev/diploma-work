//
//  AuthService.swift
//  diploma
//
//  Created by Toremurat Zholayev on 23.04.2024.
//

import Alamofire
import FirebaseAuth
import FirebaseFirestore
import CryptoKit

class AuthService {
    public static let shared = AuthService()
    
    private init() {}
    
    /// A method to register the user
    /// - Parameters:
    ///   - userRequest: The user information (email, password, username)
    ///   - completion: A completion with two values...
    ///   - Bool: wasRegistered - Deremines iif the user was restored and saved in the database correctly
    ///   - Error?:  An Optional Error if firebase provides once
    ///
    
    public func registerUser(with userRequest: RegisterUserRequest, completion: @escaping (Bool, Error?) -> Void) {
        let username = userRequest.username
        let email = userRequest.email
        let password = userRequest.password
        
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            if let error = error {
                completion(false, error)
                return
            }
            guard let resultUser = result?.user else {
                completion(false, nil)
                return
            }
            
            let db = Firestore.firestore()
            let passwordHashData = SHA256.hash(data: Data(password.utf8))
            let passwordHashString = passwordHashData.map { String(format: "%02hhx", $0) }.joined()

            
            db.collection("users")
                .document(resultUser.uid)
                .setData([
                    "username": username,
                    "email": email,
                    "passwordHash": passwordHashString
                ]) { error in
                    if let error = error {
                        completion(false, error)
                        return
                    }
                    
                    completion(true, nil)
                }
            
             
        }
    }
    
    public func signIn(with userRequest: LoginUserRequest, completion: @escaping (Error?)->Void) {
        Auth.auth().signIn(withEmail: userRequest.email, password: userRequest.password) { result, error in
            if let error = error {
                completion(error)
                return
            }
            guard let user = result?.user else {
                completion(NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: "User not found"]))
                return
            }
            user.getIDToken { token, error in
                if let error = error {
                    completion(error)
                    return
                }
                if let token = token {
                    self.saveTokenToUserDefaults(token: token)
                }
                completion(nil)
            }
        }
    }
    
    public func signOut(completion: @escaping (Error?)->Void) {
        do {
            try Auth.auth().signOut()
            completion(nil)
        } catch let error {
            completion(error)
        }
    }
    
    public func forgotPassword(with email: String, completion: @escaping (Error?) -> Void) {
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            completion(error)
        }
    }
    
    public func saveTokenToUserDefaults(token: String) {
        UserDefaults.standard.set(token, forKey: "accessToken")
    }
    
    /// A method to load the access token from UserDefaults
    /// - Returns: The access token if it exists
    public func loadTokenFromUserDefaults() -> String? {
        return UserDefaults.standard.string(forKey: "accessToken")
    }
    
    /// A method to get the current user's access token
    /// - Parameter completion: A completion with the access token or an optional error
    public func getCurrentUserToken(completion: @escaping (String?, Error?) -> Void) {
        guard let user = Auth.auth().currentUser else {
            completion(nil, NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: "User not signed in"]))
            return
        }
        user.getIDToken { token, error in
            if let error = error {
                completion(nil, error)
                return
            }
            completion(token, nil)
        }
    }
}
