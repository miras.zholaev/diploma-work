//
//  SceneDelegate.swift
//  diploma
//
//  Created by Toremurat Zholayev on 22.04.2024.
//

import UIKit
import FirebaseAuth

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        self.setupWindow(with: scene)
        self.checkAuthentication()
        
        //Jailbreak Check
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if JailbreakDetection.isJailbroken() {
                self.showJailbreakAlert()
            } else {
                self.showNotJailbrokenAlert()
            }
        }
    }

    private func setupWindow(with scene: UIScene){
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        self.window = window
        self.window?.makeKeyAndVisible()
    }
    
    public func checkAuthentication() {
        if Auth.auth().currentUser != nil && AuthService.shared.loadTokenFromUserDefaults() != nil{
            print(AuthService.shared.loadTokenFromUserDefaults()!)
            self.goToController(with: HomeController())
        } else {
            self.goToController(with: LoginController())
        }
    }
    
    private func goToController(with viewController: UIViewController) {
        DispatchQueue.main.async { [weak self] in
            UIView.animate(withDuration: 0.25) {
                self?.window?.layer.opacity = 0
            } completion: { [weak self] _ in
                let nav = UINavigationController(rootViewController: viewController)
                nav.modalPresentationStyle = .fullScreen
                self?.window?.rootViewController = nav
                
                UIView.animate(withDuration: 0.25) { [weak self] in
                    self?.window?.layer.opacity = 1
                }
            }
        }
    }
    
    private func showJailbreakAlert() {
        let alert = UIAlertController(title: "Warning", message: "This device appears to be jailbroken. For security reasons, this application cannot run on jailbroken devices.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            exit(0)
        })
        
        if let rootViewController = window?.rootViewController {
            rootViewController.present(alert, animated: true, completion: nil)
        }
    }
    
    private func showNotJailbrokenAlert() {
        let alert = UIAlertController(title: "Info", message: "This device is not jailbroken. The application can run safely.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
        if let rootViewController = window?.rootViewController {
            rootViewController.present(alert, animated: true, completion: nil)
        }
    }

}

