//
//  JailbreakDetection.swift
//  diploma
//
//  Created by Toremurat Zholayev on 11.06.2024.
//

import Foundation
import UIKit

class JailbreakDetection {
    
    static func isJailbroken() -> Bool {
        if isJailbrokenByFiles() || canWriteToSystemFiles() || canOpenJailbreakApps() || isRootAccessAvailable() {
            return true
        }
        return false
    }

    private static func isJailbrokenByFiles() -> Bool {
        let paths = ["/Applications/Cydia.app", "/Library/MobileSubstrate/MobileSubstrate.dylib"]
        for path in paths {
            if FileManager.default.fileExists(atPath: path) {
                return true
            }
        }
        return false
    }

    private static func canWriteToSystemFiles() -> Bool {
        let path = "/private/jailbreak_test.txt"
        do {
            try "Jailbreak Test".write(toFile: path, atomically: true, encoding: .utf8)
            try FileManager.default.removeItem(atPath: path)
            return true
        } catch {
            return false
        }
    }

    private static func canOpenJailbreakApps() -> Bool {
        let urlSchemes = ["cydia://package/com.example.package"]
        for scheme in urlSchemes {
            if let url = URL(string: scheme), UIApplication.shared.canOpenURL(url) {
                return true
            }
        }
        return false
    }

    private static func isRootAccessAvailable() -> Bool {
        if FileManager.default.fileExists(atPath: "/private/var/lib/apt/") {
            return true
        }
        return false
    }
}
