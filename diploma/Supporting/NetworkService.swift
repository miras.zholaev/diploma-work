//
//  NetworkService.swift
//  diploma
//
//  Created by Toremurat Zholayev on 12.06.2024.
//

import Alamofire

class NetworkService {
    static let shared = NetworkService()
    
    private init() {}
    
    func authenticatedRequest(url: String, method: HTTPMethod, parameters: Parameters?, completion: @escaping (Result<Any, Error>) -> Void) {
        guard let token = AuthService.shared.getToken() else {
            completion(.failure(NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: "Token not found"])))
            return
        }
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token)"
        ]
        
        AF.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                completion(.success(value))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func refreshToken(completion: @escaping (Bool) -> Void) {
        // Предполагается, что у вас есть API для обновления токена
        AF.request("https://yourapi.com/refresh-token", method: .post).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let json = value as? [String: Any], let newToken = json["token"] as? String {
                    AuthService.shared.saveToken(token: newToken)
                    completion(true)
                } else {
                    completion(false)
                }
            case .failure:
                completion(false)
            }
        }
    }
    
    func authenticatedRequestWithRefresh(url: String, method: HTTPMethod, parameters: Parameters?, completion: @escaping (Result<Any, Error>) -> Void) {
        authenticatedRequest(url: url, method: method, parameters: parameters) { result in
            switch result {
            case .success(let value):
                completion(.success(value))
            case .failure(let error):
                if let afError = error as? AFError, afError.responseCode == 401 {
                    // Попробуйте обновить токен и повторить запрос
                    self.refreshToken { success in
                        if success {
                            self.authenticatedRequest(url: url, method: method, parameters: parameters, completion: completion)
                        } else {
                            completion(.failure(error))
                        }
                    }
                } else {
                    completion(.failure(error))
                }
            }
        }
    }
}
